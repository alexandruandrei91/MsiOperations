#include "MsiDB.h"
#include "MsiFetchFilters.h"
#include "MsiColOperations.h"
#include "MsiTable.h"
#include <unordered_map>

unsigned int __stdcall ModifyStyles(MSIHANDLE hInstall)
{
	//MessageBox(NULL, L"ModifyStyles", L"_DEBUG", MB_OK);
	
	try
	{
		MsiDB msiDb(hInstall);

		const std::wstring textStyle{ L"RedStyle" };
		
		MsiTable controlT(msiDb, L"Control");

		MsiColOp textC(controlT, L"Text");
		MsiColOp typeC(controlT, L"Type");

		MsiQuery query = MsiQuery::SelectQuery
		(
			{ controlT },
			typeC != L"Bitmap" && IsNotNull(textC)
			//{textC} //The insertion fails if the row contains only the text.
		);
		MsiView view(msiDb, query);

		auto textIndex = view.GetMeta().IndexOf(L"Text").Get();
		while (view.Fetch())
		{
			auto currRow = view.GetCurrentRow();
			auto& currRecord = currRow[textIndex];
			auto currRecStr = currRecord.String().Get();

			//if the string is big enough to contain a style and if it begins with {\ or {&
			if (currRecStr.size() > 3 && currRecStr[0] == L'{' && (currRecStr[1] == L'\\' || currRecStr[1] == L'&'))
			{
				//delete the old style ( {\StyleName}Text blah blah -> Text blah blah )
				currRecStr.erase(currRecStr.begin(), currRecStr.begin() + currRecStr.find('}') + 1);
			}
			
			currRecord = L"{\\" + textStyle + L"}" + currRecStr;

			view.DeleteCurrRow();
			view.InsertRow(currRow, true);
		}
	}
	catch (Exception& e)
	{
		MsiUtilities::LogError(e.GetMessage());
		return e.GetCode();
	}

	return 0;
}

unsigned int __stdcall ModifyControlls(MSIHANDLE hInstall)
{
	//MessageBox(NULL, L"ModifyControlls", L"_DEBUG", MB_OK);
	
	try
	{
		MsiDB msiDb(hInstall);

		const std::wstring dialog = L"FolderDlg";
		const std::wstring comboName = L"MyCombo";

		//add combo to dialog
		MsiTable controlT(msiDb, L"Control");
		controlT.InsertRow({ dialog, L"List", L"ListBox", 130, 150, 100, 100, 65539, L"LIST", MsiCell::NullString, MsiCell::NullString, MsiCell::NullString }, true);


		//create querry to get all the id's of the controlls in the dialog
		MsiColOp dlg_c(controlT, L"Dialog_");
		MsiColOp ctrl_c(controlT, L"Control");
		MsiColOp text_c(controlT, L"Text");
		MsiColOp type_c(controlT, L"Type");


		MsiQuery query = MsiQuery::SelectQuery(
		{ controlT },
		dlg_c == dialog && ctrl_c != comboName && type_c != L"Bitmap" && IsNotNull(text_c)
		);

		MsiView view(msiDb, query);

		MsiTable coboT(msiDb, L"ListBox");
		auto index = 0;
		auto ctrlIndex = view.GetMeta().IndexOf(L"Text");

		while (view.Fetch())
		{
			auto currRow = view.GetCurrentRow();
			auto& currRecord = currRow[ctrlIndex.Get()];
			auto currRecStr = MsiUtilities::FormatString(hInstall, currRecord.String().Get()).Get();

			currRecord = currRecStr + L"(" + std::to_wstring(currRecStr.size()) + L")";

			view.DeleteCurrRow();
			view.InsertRow(currRow, true);

			//insert in list
			coboT.InsertRow({ L"LIST", index++, currRecord.String().Get(), currRecord.String().Get() }, true);
		}
	}
	catch (Exception& e)
	{
		return e.GetCode();
	}

	return 0;
}

unsigned int __stdcall ListControlls(MSIHANDLE hInstall)
{
	//MessageBox(NULL, L"ListControlls", L"_DEBUG", MB_OK);

	try
	{
		MsiDB msiDb(hInstall);

		const std::wstring dialog = L"WelcomeDlg";
		const std::wstring listName = L"MyList";

		//add list to dialog
		MsiTable controlT(msiDb, L"Control");
		controlT.InsertRow({ dialog, listName, L"ListBox", 250, 125, 100, 100, 65539, L"LIST", MsiCell::NullString, MsiCell::NullString, MsiCell::NullString }, true);

		//create querry to get the first control from the dialog
		MsiTable dialogT(msiDb, L"Dialog");
		MsiColOp dlg_d(dialogT, L"Dialog");
		MsiColOp ctrlFirst_d(dialogT, L"Control_First");

		MsiColOp dlg_c(controlT, L"Dialog_");
		MsiColOp ctrl_c(controlT, L"Control");
		MsiColOp ctrlNext_c(controlT, L"Control_Next");

		MsiQuery queryFirst = MsiQuery::SelectQuery(
		{ dialogT, controlT },
		dlg_d == dialog && dlg_c == dlg_d && ctrl_c == ctrlFirst_d,
		{ ctrl_c, ctrlNext_c }
		);

		MsiView viewFirst(msiDb, queryFirst);
		viewFirst.Fetch();
		auto firstRow = viewFirst.GetCurrentRow();
		auto ctrlIndex = viewFirst.GetMeta().IndexOf(L"Control").Get();
		auto nextIndex = viewFirst.GetMeta().IndexOf(L"Control_Next").Get();

		std::wstring ctrlFirst = firstRow[ctrlIndex].String().Get();
		std::wstring prevNext = firstRow[nextIndex].String().Get();

		//Construct map with control elements. The key is the current control, the value is the next control.
		std::unordered_map<std::wstring, std::wstring> controls;
		controls[ctrlFirst] = prevNext;

		
		//Select all the controls of a dialog.
		MsiQuery queryRest = MsiQuery::SelectQuery(
		{ controlT },
		dlg_c == dialog,
		{ ctrl_c, ctrlNext_c }
		);

		MsiView view(msiDb, queryRest);
		ctrlIndex = view.GetMeta().IndexOf(L"Control").Get();
		nextIndex = view.GetMeta().IndexOf(L"Control_Next").Get();

		while (view.Fetch())
		{
			const auto& currentRow = view.GetCurrentRow();
			auto curr = currentRow[ctrlIndex].String().Get();
			auto next = currentRow[nextIndex].String().Get();

			controls[curr] = next;
		}

		//insert the elements from the map in the ListBox.
		auto index = 1;
		MsiTable coboT(msiDb, L"ListBox");
		for (int i = 0; i < controls.size() - 1; i++) //we go only to size-1 because the last element goes back to the first.
		{
			static auto elem = ctrlFirst;
			coboT.InsertRow({ L"LIST", index++, elem, elem}, true);
			elem = controls[elem];
		}
	}
	catch (Exception& e)
	{
		return e.GetCode();
	}

	return 0;
}

unsigned int __stdcall InstalledFilesTest(MSIHANDLE hInstall)
{
	//MessageBox(NULL, L"InstalledFilesTest", L"_DEBUG", MB_OK);

	try
	{
		MsiDB msiDb(hInstall);

		//The directory we want to check for the installed files
		const std::wstring dir = L"APPDIR";

		MsiTable compT(msiDb, L"Component");
		MsiTable dirT(msiDb, L"Directory");
		MsiTable fileT(msiDb, L"File");

		MsiColOp compC(compT, L"Component");
		MsiColOp dirC(compT, L"Directory_");

		MsiColOp dirD(dirT, L"Directory");

		MsiColOp fileF(fileT, L"File");
		MsiColOp compF(fileT, L"Component_");

		//Get all the files (and the components they're part of) from a specific directory
		MsiQuery query = MsiQuery::SelectQuery
		(
			{ compT, dirT, fileT },
			dirD == dir && dirC == dirD && compF == compC,
			{ compC, fileF }
		);
		MsiView msiview(msiDb, query);

		//from the all the files of a directory, filter only those that will be installed
		MsiFetchFilters filters{
			[=](const MsiMeta& meta, const MsiRow& row) -> bool
			{
				auto compIndex = meta.IndexOf(L"Component");
				auto comp = row[compIndex.Get()].String();
				//return if the component will be installed locally: ($CompName=3)
				return MsiUtilities::ConditionEvaluator(hInstall, L"($" + comp.Get() + L"=3)").Get();
			}
		};

		//insert the results in a list box
		MsiTable(msiDb, L"Control").InsertRow({ L"WelcomeDlg", L"MyList", L"ListBox", 250, 125, 100, 100, 65539, L"LIST", MsiCell::NullString, MsiCell::NullString, MsiCell::NullString }, true);
		MsiTable listT(msiDb, L"ListBox");

		while (msiview.Fetch(filters))
		{
			auto index = msiview.GetMeta().IndexOf(L"File");
			auto fileName = msiview.GetCurrentRow()[index.Get()].String();

			MsiUtilities::LogInfo(fileName.Get());

			static int listOrder = 0;
			listT.InsertRow({ L"LIST", ++listOrder, fileName.Get(), fileName.Get() }, true);
		}
	}
	catch (Exception& e)
	{
		MsiUtilities::LogError(e.GetMessage());
		return e.GetCode();
	}

	return 0;
}