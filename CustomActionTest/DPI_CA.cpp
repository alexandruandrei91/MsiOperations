#include "MsiDB.h"
#include "MsiUtilities.h"
#include "Maybe.h"
#include "MsiTable.h"
#include "MsiColOperations.h"

bool QuerySystemDPI(int * aDPI, int * aPercent)
{
	const int kDefaultDPI = 96; // system default DPI (ppi)
	int dpi = ::GetDeviceCaps(::GetDC(::GetDesktopWindow()), LOGPIXELSY);

	if (aDPI)
		*aDPI = dpi;
	if (aPercent)
		*aPercent = (100 * dpi) / kDefaultDPI;

	return (dpi == kDefaultDPI);
}

enum BitmapDisplayMode
{
	BmpDisplayDpiAdaptive = 0, // if the system uses a higher-DPI, stretch image to fit control bounds
	BmpDisplayControlIntrinsic = 1, // control attribute (FixedSize) dictates behavior (we DO NOTHING)
	BmpDisplayAlwaysFixedSize = 2, // always maintain original image size
	BmpDisplayAlwaysStretch = 3, // always stretch image to fit control bounds
	BmpDisplayDefault = BmpDisplayDpiAdaptive
};

unsigned int __stdcall BmpSizeDpi(MSIHANDLE hInstall)
{
	//MessageBox(NULL, L"BmpSizeDpi", L"_DEBUG", MB_OK);

	try
	{
		MsiDB msiDb(hInstall);

		const BitmapDisplayMode mode = BmpDisplayDpiAdaptive;
		const unsigned int fixedSizeFlag = 1048576;

		MsiTable controlT(msiDb, L"Control");

		MsiColOp typeC(controlT, L"Type");
		MsiColOp attrC(controlT, L"Attributes");

		MsiQuery query = MsiQuery::SelectQuery
		(
			{ controlT },
			typeC == L"Bitmap"
			//{attrC} //The insertion fails if the row contains only the text.
		);
		MsiView view(msiDb, query);

		auto textIndex = view.GetMeta().IndexOf(L"Attributes").Get();
		while (view.Fetch())
		{
			auto currRow = view.GetCurrentRow();
			auto& currRecord = currRow[textIndex];
			auto attr = currRecord.Int().Get();

			switch (mode)
			{
			case BmpDisplayDpiAdaptive:
				//if the dpi is scaled, remove fixedSizeFlag, else leave it as it is
				if (!QuerySystemDPI(nullptr, nullptr))
					currRecord = attr & ~fixedSizeFlag;
				break;
			case BmpDisplayControlIntrinsic:
				//do noting; here just for completion
				break;
			case BmpDisplayAlwaysFixedSize:
				currRecord = attr | fixedSizeFlag;
				break;
			case BmpDisplayAlwaysStretch:
				currRecord = attr & ~fixedSizeFlag;
				break;
			}

			view.DeleteCurrRow();
			view.InsertRow(currRow, true);
		}
	}
	catch (Exception& e)
	{
		MsiUtilities::LogError(e.GetMessage());
		return e.GetCode();
	}

	return 0;
}
