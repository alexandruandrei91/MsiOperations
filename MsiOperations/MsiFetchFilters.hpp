template<typename ...Args>
MsiFetchFilters::MsiFetchFilters(CallableFilter pFilter, Args&&... pFilters)
{
	AddFilters(std::move(pFilter), std::forward<Args>(pFilters)...);
}

template<typename ...Args>
void MsiFetchFilters::AddFilters(CallableFilter pFilter, Args&&... pFilters)
{
	mFilters.push_back(std::move(pFilter));
	AddFilters(std::forward<Args>(pFilters)...);
}