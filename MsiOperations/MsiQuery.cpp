#include "MsiQuery.h"
#include "MsiTable.h"
#include "MsiColOperations.h"

MsiQuery::MsiQuery(std::wstring pQueryStr)
	: mQueryStr(std::move(pQueryStr))
{

}

void MsiQuery::SetString(std::wstring pQueryStr)
{
	mQueryStr = std::move(pQueryStr);
}

const std::wstring& MsiQuery::GetQuery() const
{
	return mQueryStr;
}

void MsiQuery::OrderBy(std::wstring pColName)
{
	mOrderBy.push_back(std::move(pColName));

	UpdateQuery();
}

void MsiQuery::OrderBy(std::vector<std::wstring> pColNames)
{
	mOrderBy.insert(mOrderBy.begin(), pColNames.begin(), pColNames.end());

	UpdateQuery();
}

MsiQuery MsiQuery::CreateTableQuery(const std::wstring& pTableName, const MsiMeta& pMeta)
{
	std::wstring query(L"CREATE TABLE " + pTableName + L"( ");

	//appends to the string all the column names and types
	auto colCount = pMeta.ColCount();
	for (unsigned int i = 1; i <= colCount; i++)
	{
		query += pMeta[i].Name();
		query += L" ";
		query += pMeta[i].GetSQLType();
		if (i != colCount)
		{
			query += L", ";
		}
	}

	//appends to the string the primary keys
	query += L" PRIMARY KEY ";
	auto pks = pMeta.GetPrimaryKeys();
	for (auto pk = std::begin(pks); pk < std::prev(std::end(pks)); pk++)
	{
		query += pk->Name();
		query += L", ";
	}
	//after the last element, no comma is needed.
	query += pks.back().Name();
	
	query += L")";

	return MsiQuery(query);
}

MsiQuery MsiQuery::AddColumnQuery(const std::wstring& pTableName, const MsiColInfo& pColInfo)
{
	return MsiQuery(L"ALTER TABLE " + pTableName + L" ADD " + pColInfo.Name() + L" " + pColInfo.GetSQLType());
}

void MsiQuery::UpdateQuery()
{
	if (mOrderBy.size())
		mQueryStr += L" ORDER BY ";
	for (auto orderBy = std::begin(mOrderBy); orderBy != std::prev(mOrderBy.end()); orderBy++)
	{
		mQueryStr += *orderBy;
		mQueryStr += L", ";
	}
	mQueryStr += mOrderBy.back();
}

MsiQuery MsiQuery::SelectQuery(
	const std::vector<std::wstring>& pTables,
	const std::wstring& pOp,
	const std::vector<std::wstring>& pCols)
{
	std::wstring query(L"SELECT ");

	if (pCols.size())
	{
		for (auto col = pCols.begin(); col < std::prev(pCols.end()); col++)
		{
			query += *col;
			query += L", ";
		}
		query += pCols.back();
	}
	else
	{
		query += L"*";
	}

	query += L" FROM ";

	for (auto table = pTables.begin(); table < std::prev(pTables.end()); table++)
	{
		query += *table;
		query += L", ";
	}

	query += pTables.back();

	if (!pOp.empty())
	{
		query += L" WHERE " + pOp;
	}

	return query;
}

MsiQuery MsiQuery::SelectQuery(
	const std::vector<std::reference_wrapper<MsiTable>>& pTables,
	const std::vector<MsiColOp>& pCols)
{
	std::vector<std::wstring> tablesNames;
	tablesNames.reserve(pTables.size());
	for (auto table : pTables)
		tablesNames.push_back(table.get().GetName());

	std::vector<std::wstring> colsNames;
	colsNames.reserve(pCols.size());
	for (auto col : pCols)
		colsNames.push_back(col.GetStr());

	return SelectQuery(tablesNames, L"", colsNames);
}

MsiQuery MsiQuery::SelectQuery(
	const std::vector<std::reference_wrapper<MsiTable>>& pTables,
	const MsiColOperations& pOp,
	const std::vector<MsiColOp>& pCols)
{
	std::vector<std::wstring> tablesNames;
	tablesNames.reserve(pTables.size());
	for (auto table : pTables)
		tablesNames.push_back(table.get().GetName());

	std::vector<std::wstring> colsNames;
	colsNames.reserve(pCols.size());
	for (auto col : pCols)
		colsNames.push_back(col.GetStr());

	return SelectQuery(tablesNames, pOp.GetStr(), colsNames);
}
