template<typename... Args>
MsiCells::MsiCells(MsiCell pCell, Args&&... pCells)
{
	AddCells(std::move(pCell), std::forward<Args>(pCells)...);
}

template<typename T, typename... Args>
void MsiCells::AddCells(T&& pCell, Args&&... pCells)
{
	mCells.emplace_back(pCell);
	AddCells(std::forward<Args>(pCells)...);
}