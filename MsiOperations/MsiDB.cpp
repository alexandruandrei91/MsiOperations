#include "MsiDB.h"
#include "MsiUtilities.h"
#include "MsiTable.h"
#include "Maybe.h"
#include <MsiQuery.h>

MsiDB::MsiDB(std::wstring pMsiPath)
	: mMsiPath(std::move(pMsiPath))
{
	MsiUtilities::Verify(MsiOpenDatabase(mMsiPath.c_str(), MSIDBOPEN_TRANSACT, &mDBHandle));
}

MsiDB::MsiDB(MSIHANDLE pInstallHandle)
{
	mDBHandle = MsiGetActiveDatabase(pInstallHandle);
	if (!mDBHandle)
	{
		MsiUtilities::CriticalError(L"MsiGetActiveDatabase didn't returned a valid handle!");
	}
}

MsiDB::~MsiDB()
{
	//Only log if they fail
	MsiUtilities::Verify(MsiDatabaseCommit(mDBHandle), false);
	MsiUtilities::Verify(MsiCloseHandle(mDBHandle), false);
}

MSIHANDLE MsiDB::GetHandle() const
{
	return mDBHandle;
}

bool MsiDB::TableExists(const std::wstring& pTableName) const
{
	MsiView view(*this, MsiQuery(L"SELECT * FROM _Tables WHERE Name='" + pTableName + L"'"));
	return view.GetRowsCount() != 0;
}

std::vector<std::wstring> MsiDB::GetAllTablesNames() const
{
	MsiView tables(*this, MsiQuery(L"SELECT * FROM _Tables"));
	auto rows = tables.GetAllRows();

	std::vector<std::wstring> result;
	result.reserve(rows.size());
	for (const auto& row : rows)
	{
		if (auto r = row[1].String())
			result.push_back(r.Get());
	}

	return result;
}

std::vector<MsiDB::MsiSummaryInfo> MsiDB::GetSummaryInfo()
{
	PMSIHANDLE summaryH;
	MsiUtilities::Verify(MsiGetSummaryInformation(mDBHandle, nullptr, 1, &summaryH));

	unsigned int propCount = 0;
	MsiUtilities::Verify(MsiSummaryInfoGetPropertyCount(summaryH, &propCount));

	std::vector<MsiSummaryInfo> summaryInfo(propCount);
	unsigned int propIndex = 1;
	for (auto& info : summaryInfo)
	{
		unsigned long strSize = 0;
		auto res = MsiUtilities::Verify(MsiSummaryInfoGetProperty(
				summaryH,
				propIndex++,
				&info.DataType,
				&info.IntVal,
				&info.FileVal,
				L"",
				&strSize
			),
			false);

		if (!res)
		{
			if (res.GetError().GetCode() == ERROR_MORE_DATA)
			{
				wchar_t* strVal = new wchar_t[strSize+1];

				MsiUtilities::Verify(MsiSummaryInfoGetProperty
				(
					summaryH,
					propIndex++,
					&info.DataType,
					&info.IntVal,
					&info.FileVal,
					strVal,
					&strSize
				));
				strVal[strSize] = '\0';
				info.StrVal = strVal;
				delete[] strVal;
			}
			else
			{
				MsiUtilities::CriticalError(res.GetError());
			}
		}
	}

	MsiUtilities::Verify(MsiSummaryInfoPersist(summaryH));
	return summaryInfo;
}

void MsiDB::SetSummaryInfo(std::vector<MsiSummaryInfo> pSummaryInfo)
{
	PMSIHANDLE summaryH;
	MsiUtilities::Verify(MsiGetSummaryInformation(mDBHandle, nullptr, 0, &summaryH));

	unsigned int propIndex = 1;
	for (auto& info : pSummaryInfo)
	{
		MsiUtilities::Verify(MsiSummaryInfoSetProperty
		(
			summaryH,
			propIndex++,
			info.DataType,
			info.IntVal,
			&info.FileVal,
			info.StrVal.c_str()
		));
}

	MsiUtilities::Verify(MsiSummaryInfoPersist(summaryH));
}