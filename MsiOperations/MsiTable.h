#pragma once
#include "MsiView.h"
#include <memory>

/** @file */

/**
*It represents a view over an entire table with some specific operations.
*
*This class is a view over a table that offers the possibility to create a table
*in the Msi Database, delete it or add columns. Unlike a normal view, the primary
*keys can be retrieved from a table's metadata.
*@see MsiView
*@see MsiMeta
*/
class MSI_EXPORT MsiTable : public MsiView
{
public:
	/**
	*Class Constructor.
	*
	*It tries to open the table pName from the database pDb. If the table
	*doesn't exists, the program exits.
	*@param pDb A reference to the database.
	*@param pName The table's name.
	*@remark The MsiDB object must stay alive at least as long as the MsiTable (a reference to it
	*is hold inside the table).
	*@see MsiDB::TableExists(const std::wstring& pTableName) const
	*/
	MsiTable(const MsiDB& pDb, std::wstring pName);

	/**
	*Class Constructor.
	*
	*It tries to create the table pName in the database pDb using the pMeta schema. If the table
	*already exists, the program exits.
	*@param pDb A reference to the database.
	*@param pName The table's name.
	*@param pMeta The schema (metadata) of the table to be created.
	*@remark The MsiDB object must stay alive at least as long as the MsiTable (a reference to it
	*is hold inside the table).
	*@see MsiDB::TableExists(const std::wstring& pTableName) const
	*/
	MsiTable(const MsiDB& pDb, std::wstring pName, MsiMeta pMeta);

	/**
	*Deleted copy constructor.
	*/
	MsiTable(const MsiTable&) = delete;
	
	/**
	*Deleted copy assignment operator.
	*/
	MsiTable& operator=(const MsiTable&) = delete;

	/**
	*Move Constructor.
	*
	*@param pOther The object to be moved in this.
	*/
	MsiTable(MsiTable&& pOther);
	
	/**
	*Move assignment operator.
	*
	*@param pOther The object to be moved in this.
	*/
	MsiTable& operator=(MsiTable&& pOther);

	/**
	*Adds a column to the table in the Msi database.
	*
	*@param pCol Column information of the column to be added in the table.
	*@remark The column must be nullable.
	*/
	void AddColumn(MsiColInfo pCol);

	/**
	*Delete the table from the database.
	*/
	void DropTable();

	/**
	*Specify the column to order by.
	*
	*@param pColName The column to order by.
	*@remark Unlike MsiQuery::OrderBy(std::wstring pColName), calling this function multiple times,
	*will <b>NOT</b> accumulate the columns to order by.
	*@see MsiQuery::OrderBy(std::wstring pColName)
	*/
	void OrderBy(std::wstring pColName);

	/**
	*Specify the column to order by.
	*
	*@param pColNames The column to order by.
	*@remark Unlike MsiQuery::OrderBy(std::vector<std::wstring> pColNames) calling this function multiple times,
	*will <b>NOT</b> accumulate the columns to order by.
	*@see MsiQuery::OrderBy(std::vector<std::wstring> pColNames)
	*/
	void OrderBy(std::vector<std::wstring> pColNames);

	/**
	*Getter for the table's name.
	*
	*@return The table's name.
	*/
	const std::wstring& MsiTable::GetName() const;

private:
	void FillPrimaryKeys();

	const MsiDB& mMsiDb;
	std::wstring mName;
};
