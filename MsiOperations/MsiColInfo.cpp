#include "MsiColInfo.h"

MsiColInfo::MsiColInfo()
	: mName(L""), mType(MsiDataType::NONE), mSize(0), mPrimary(false), mNullable(true), mLocalizable(false)
{

}

MsiColInfo::MsiColInfo(std::wstring pName, MsiDataType pType, unsigned int pSize, bool pPrimary, bool pNullable, bool pLocalizable)
{
	//in the setters, there are constraints enforced
	Name(pName);
	Type(pType);
	Size(pSize);
	IsPrimaryKey(pPrimary);
	IsNullable(pNullable);
	IsLocalizable(pLocalizable);
}

const std::wstring& MsiColInfo::Name() const
{
	return mName;
}

void MsiColInfo::Name(std::wstring pName)
{
	mName = std::move(pName);
}

MsiDataType MsiColInfo::Type() const
{
	return mType;
}

void MsiColInfo::Type(MsiDataType pType)
{
	mType = pType;
}

unsigned int MsiColInfo::Size() const
{
	return mSize;
}

void MsiColInfo::Size(unsigned int pSize)
{
	if (pSize && mType != MsiDataType::STRING)
		MsiUtilities::LogError(L"Only values of type String can have size!");
	else
		mSize = pSize;
}

bool MsiColInfo::IsPrimaryKey() const
{
	return mPrimary;
}

void MsiColInfo::IsPrimaryKey(bool pPrimary)
{
	if (pPrimary && mNullable)
		MsiUtilities::LogError(L"A nullable column cannot be primary key!");
	else
		mPrimary = pPrimary;
}

bool MsiColInfo::IsNullable() const
{
	return mNullable;
}

void MsiColInfo::IsNullable(bool pNullable)
{
	if (pNullable && mPrimary)
		MsiUtilities::LogError(L"A column marked as primary key cannot be nullable!");
	else
		mNullable = pNullable;
}

bool MsiColInfo::IsLocalizable() const
{
	return mLocalizable;
}

void MsiColInfo::IsLocalizable(bool pLocalizable)
{
	mLocalizable = pLocalizable;
}

std::wstring MsiColInfo::GetSQLType() const
{
	std::wstring type;

	switch (mType)
	{
	case MsiDataType::INTEGER:
		type = L"LONG";
		break;
	case MsiDataType::STRING:
		if (mSize == 0)
		{
			type = L"LONGCHAR";
		}
		else
		{
			type = L"CHAR(" + std::to_wstring(mSize) + L")";
		}
		break;
	case MsiDataType::STREAM:
		type = L"OBJECT";
	}

	if (!mNullable)
	{
		type += L" NOT NULL";
	}

	if (mLocalizable)
	{
		type += L" LOCALIZABLE";
	}

	return type;
}