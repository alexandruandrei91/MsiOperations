#pragma once
#include "MsiQuery.h"
#include "MsiDB.h"
#include "MsiMeta.h"
#include "MsiCells.h"
#include "MsiFetchFilters.h"
#include <Msi.h>

/** @file */

/**
*Represents a view into the Msi Database
*
*A view represents an interrogation result, so this class represents the result
*for queries, may they be to fetch, insert or modify data. The operations on a MsiView are done
*trough a cursor, a current row (MsiRow), in order to prevent loading all the database in memory.
*Fetching values using this class resembles reading a file using the standard libraries (while(!f.eof) f.read()).
*For more informations, the 
*<a href="https://msdn.microsoft.com/en-us/library/aa368250(v=vs.85).aspx">MSDN documentation</a>
*may be helpful.
*/
class MSI_EXPORT MsiView
{
public:
	/**
	*Class Constructor.
	*
	*Execute a query over a Msi Database and save the resulting view.
	*@param pDb The database on which we query.
	*@param pQuery The query to execute.
	*@see MsiDB
	*@see MsiQuery
	*/
	MsiView(const MsiDB& pDb, const MsiQuery& pQuery);

	/**
	* Deleted copy constructor.
	*/
	MsiView(const MsiView&) = delete;

	/**
	*Deleted copy assignment operator.
	*/
	MsiView& operator=(const MsiView&) = delete;

	/**
	*Move Constructor.
	*
	*@param pOther The object to be moved in this.
	*/
	MsiView(MsiView&& pOther);

	/**
	*Move assignment operator.
	*
	*@param pOther The object to be moved in this.
	*/
	MsiView& operator=(MsiView&& pOther);

	/**
	*The virtual destructor, overloaded by the derived classes.
	*/
	virtual ~MsiView();

	/**
	*Getter for the view's metadata.
	*@return View's schema (metadata).
	*@see MsiMeta
	*/
	const MsiMeta& GetMeta() const;

	/**
	*Check if the view reached the end.
	*
	*If the view reached the end (the last record was fetched),
	*this function will return false.
	*@retval true The view reached the end.
	*@retval false The view did not reached the end.
	*/
	bool EndOfView() const;

	/**
	*Fetch the next element.
	*
	*By calling this function, the next record that match the filter will be read in the current row.
	*If the view reached the end, this function will return false. Get the result
	*of Fetch by using GetCurrentRow() const. If this function fails, the program will exit.
	*@param pFilters [default = MsiFetchFilters()] The filter applied on the fetch.
	*@retval true More values are available for fetching
	*@retval false The view reached the end.
	*@see MsiFetchFilters
	*@see GetCurrentRow() const
	*@see EndOfView() const
	*/
	bool Fetch(MsiFetchFilters pFilters = MsiFetchFilters());

	/**
	*Resets the view.
	*
	*It sets the cursor to the beginning of the view.
	*/
	void Reset();

	/**
	*Gets the row from the current position.
	*
	*@return The current row in the view.
	*@see MsiRow
	*/
	const MsiRow& GetCurrentRow() const;

	/**
	*Inserts a row at the current position.
	*
	*@param pRow Row to be inserted.
	*@param pTemp [default=false] If true, insert a temporary row.
	*@see MsiRow
	*/
	void InsertRow(MsiRow pRow, bool pTemp = false);

	/**
	*Updates the row at the current position.
	*
	*The current row will be replaced by the row received.
	*@param pRow Row to be inserted.
	*@param pCreate [default = true] If pCreate is true, create the row if it doesn't exists.
	*@remark The primary keys cannot be updated.
	*@see MsiRow
	*/
	void UpdateCurrRow(MsiRow pRow, bool pCreate = true);

	/**
	*Delete the current row from the view.
	*/
	void DeleteCurrRow();

	/**
	*Get all rows in the view.
	*
	*@return A vector of MsiRows containing all records from the view.
	*@remark This function is heavy on memory, i should be used only for debugging purposes!
	*@see MsiRow
	*/
	std::vector<MsiRow> GetAllRows();

	/**
	*Get the index of the current row.
	*
	*@return Index of the current row.
	*@remark the index starts from 1.
	*/
	unsigned int GetCurrRowIndex();

	/**
	*Get the number of rows in the view.
	*
	*@return Number of rows in the view.
	*/
	unsigned int GetRowsCount();

	/**
	*Go to a certain index in a view.
	*
	*Fetch from the beginning of the view until a certain index.
	*@param pIndex The index of the row to go.
	*/
	void GoToRow(unsigned int pIndex);

protected:
	/**
	*Default Constructor
	*
	*Is marked as protected because only the derived classes can create
	*a view and execute a query later.
	*/
	MsiView();

	/**
	*Execute a query over a Msi Database and save the resulting view.
	*
	*@param pDb The database on which we query.
	*@param pQuery The query to execute.
	*@see MsiDB
	*@see MsiQuery
	*/
	void Execute(const MsiDB& pDb, const MsiQuery& pQuery);

	/**
	*Parse the view and fills the metadata member.
	*/
	void FillMeta();

	/**
	*Gets a string from a record.
	*
	*@param pHandle The record handle, from which we extract the string.
	*@param pField The index of record's cell that contains the string.
	*@return If the string extraction was successful, in the Maybe will be a string,
	*otherwise it will be an Exception.
	*@see Maybe
	*/
	Maybe<std::wstring> GetRecordString(MSIHANDLE pHandle, unsigned int pField) const;

	/**
	*The Metadata for this view.
	*@see MsiMeta
	*/
	MsiMeta mMeta;
private:
	void UpdateRow();
	void SetStreamCell(unsigned int pCellIndex);
	bool IsValid(const MsiRow& pRow);

	void FillType(MsiColInfo& pColInfo, const std::wstring& pType);
	
	void CloseHandles();

	Maybe<std::vector<char>> GetRecordStream(MSIHANDLE pHandle, unsigned int pField) const;

	MSIHANDLE mViewHandle;

	MSIHANDLE mCurrentRowH;
	MsiRow mCurrentRow;
	bool mEndOfViewFlag;

	unsigned int mCurrPos;
};
