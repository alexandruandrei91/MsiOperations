#include "Maybe.h"

Exception::Exception(unsigned int pErrorCode, std::wstring pErrorMessage, std::unique_ptr<Exception> pInnerException)
	: mErrorCode(pErrorCode), mErrorMessage(std::move(pErrorMessage)), mInnerException(std::move(pInnerException))
{

}

Exception::Exception(CustomErrorCode pErrorCode, std::wstring pErrorMessage, std::unique_ptr<Exception> pInnerException)
	: mErrorCode(static_cast<unsigned int>(pErrorCode)), mErrorMessage(std::move(pErrorMessage)), mInnerException(std::move(pInnerException))
{

}

Exception::Exception(Exception&& pOther)
	: mErrorCode(std::move(pOther.mErrorCode)), mErrorMessage(std::move(pOther.mErrorMessage)), mInnerException(std::move(pOther.mInnerException))
{
	pOther.mErrorCode = ERROR_SUCCESS;
}

Exception::Exception(const Exception& pOther)
	: mErrorCode(pOther.mErrorCode), mErrorMessage(pOther.mErrorMessage)
{
	if (pOther.mInnerException)
		mInnerException = std::make_unique<Exception>(*(pOther.mInnerException));
}

Exception& Exception::operator=(Exception&& pOther)
{
	if (this != &pOther)
	{
		mErrorCode = std::move(pOther.mErrorCode);
		pOther.mErrorCode = ERROR_SUCCESS;
		mErrorMessage = std::move(pOther.mErrorMessage);
		mInnerException = std::move(pOther.mInnerException);
	}

	return *this;
}

Exception& Exception::operator=(const Exception& pOther)
{
	if (this != &pOther)
	{
		mErrorCode = pOther.mErrorCode;
		mErrorMessage = pOther.mErrorMessage;
		mInnerException = std::move(std::make_unique<Exception>(*pOther.mInnerException));
	}

	return *this;
}

unsigned int Exception::GetCode() const
{
	return mErrorCode;
}

const std::wstring& Exception::GetMessage() const
{
	return mErrorMessage;
}

const Exception* const Exception::GetInnerException() const
{
	return mInnerException.get();
}

std::wstring Exception::Str() const
{
	return L"[" + std::to_wstring(mErrorCode) + L"]" + mErrorMessage;
}