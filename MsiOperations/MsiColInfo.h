#pragma once
#include "MsiUtilities.h"
#include <string>

/** @file */

/**
*Keeps informations about each column in a MsiTable.
*
*This class holds informations about each cell of a MsiTable, such as
*it's name, type, data size, if it's a primary key, nullable or localizable.
*It also provides getters and setters for this data and a getter to retrieve
*the column information in SQL format.
*
*@see MsiMeta
*/
class MSI_EXPORT MsiColInfo
{
public:
	/**
	*Default constructor.
	*
	*It initialize all the data to default values.
	*@see MsiColInfo(std::wstring pName, MsiDataType pType, unsigned int pSize, bool pPrimary, bool pNullable, bool pLocalizable)
	*/
	MsiColInfo();

	/**
	*Class constructor.
	*
	*It sets all the information needed by a cell.
	*@param pName The column's name.
	*@param pType The column's type.
	*@param pSize The column's data size. Used for strings with limited number of characters.
	*@param pPrimary true if the column is part of the primary key, false otherwise.
	*@param pNullable true if the column is nullable, false otherwise. Cannot be true if the column is part of the Primary Key.
	*@param pLocalizable true if the column is localizable, false otherwise. Applicable for strings.
	*/
	MsiColInfo(std::wstring pName, MsiDataType pType, unsigned int pSize, bool pPrimary, bool pNullable, bool pLocalizable);

	/**
	*Getter for the column's name.
	*
	*@return Column's name.
	*/
	const std::wstring& Name() const;

	/**
	*Setter for the column's name.
	*
	*@param pName Column's name.
	*@remark The argument is sent by value because it's moved into the internal member.
	*/
	void Name(std::wstring pName);

	/**
	*Getter for the column's type.
	*
	*@return Column's type.
	*@see MsiDataType
	*/
	MsiDataType Type() const;

	/**
	*Setter for the column's type.
	*
	*@param pType Column's type.
	*@see MsiDataType
	*/
	void Type(MsiDataType pType);

	/**
	*Getter for the column's size (for integers, streams and variable strings, it will be 0). 
	*
	*@return Column's size.
	*/
	unsigned int Size() const;

	/**
	*Setter for the column's size (for integers, streams and variable strings, it should be 0). 
	*
	*@param pSize Column's size.
	*/
	void Size(unsigned int pSize);

	/**
	*Getter for the column's primary key attribute.
	*
	*@retval true The column a primary key or part of a composed primary key.
	*@retval false The column is not a primary key.
	*/
	bool IsPrimaryKey() const;

	/**
	*Setter for the column's primary key attribute.
	*
	*@param pPrimary Set if the column is primary key or not.
	*/
	void IsPrimaryKey(bool pPrimary);

	/**
	*Getter for the column's nullable attribute.
	*
	*@retval true The column can be null.
	*@retval false The column cannot be null.
	*/
	bool IsNullable() const;

	/**
	*Setter for the column's nullable attribute.
	*
	*@param pNullable Set if the column is nullable or not.
	*/
	void IsNullable(bool pNullable);

	/**
	*Getter for the column's localizable attribute. Applicable to only to strings.
	*
	*@retval true The column is localizable.
	*@retval false The column is not localizable.
	*/
	bool IsLocalizable() const;

	/**
	*Setter for the column's localizable attribute. Applicable to only to strings.
	*
	*@param pLocalizable Set if the column is localizable or not.
	*/
	void IsLocalizable(bool pLocalizable);

	/**
	*Getter for the column's information in SQL format.
	*
	*This getter is used to transform the column's information in Msi Column Definition Format
	*in order to send it to the Msi Database when creating/modifying a column.
	*See <a href="https://msdn.microsoft.com/en-us/library/aa367870(v=vs.85).aspx">Msi Documentation</a>.
	*@return A string with the Msi Column Definition Format.
	*/
	std::wstring GetSQLType() const;
	
private:
	std::wstring mName;
	MsiDataType mType;
	unsigned int mSize;
	bool mPrimary;
	bool mNullable;
	bool mLocalizable;
};
