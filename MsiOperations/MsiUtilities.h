#pragma once
#include <string>

#ifdef MSI_OPERATIONS_EXPORT
#define MSI_EXPORT __declspec(dllexport)
#else
#define MSI_EXPORT
#endif

/**@file*/

class Exception;
//forward declaration to avoid circular inclusion
template<typename T>
class Maybe;

/**
*Enum of data types found in a Msi Database.
*/
enum class MsiDataType
{
	INTEGER,	/**< The value is an integer. */
	STREAM,		/**< The value is a stream. */
	STRING,		/**< The value is a string. */
	NONE		/**< Used for initialization. @see MsiCell. */
};

/**
*Utility class containing static helper methods such as logging and error checking.
*
*@todo Write logs in the console/output/file depending on a define
*/
class MSI_EXPORT MsiUtilities
{
public:

	/**
	*Prints an information message to the MSI log.
	*
	*@param pInstallHandle The install MSIHANDLE
	*@param pMessage The message to log.
	*/
	static void LogMsi(unsigned int pInstallHandle, const std::wstring& pMessage);

	/**
	*Prints an information message to wclog and visual studio output window.
	*
	*@param pMessage The message to log.
	*/
	static void LogInfo(const std::wstring& pMessage);

	/**
	*Prints an error message to wcerr and visual studio output window.
	*
	*@param pMessage The message to log.
	*/
	static void LogError(const std::wstring& pMessage);

	/**
	*Prints an error message then throws and Exception.
	*
	*@param pMessage The message to log.
	*@remark This function throws an Exception.
	*/
	static void CriticalError(const std::wstring& pMessage);

	/**
	*Prints an error message then throws the Exception.
	*
	*@param pException The exception to log and throw.
	*@remark This function throws an Exception.
	*/
	static void CriticalError(const Exception& pException);

	/**
	*Verify an error code.
	*
	*Method used mainly to verify windows api calls. If the error code differs from ERROR_SUCCESS,
	*the function either closes the program or returns an error in a Maybe<void>, depending if mCritical
	*is true or false. If the error code is ERROR_SUCCESS, the function returns a Maybe<void> that will cast to true.
	*<p>
	*The following code will verify MsiViewExecute. If it fails, the program will exit.
	*@code
	*	MsiUtilities::Verify(MsiViewExecute(mViewHandle, NULL));
	*@endcode
	*The following code will also verify MsiViewExecute but this time, if it fails, it will return a Maybe<void> containing an error.
	*When the Maybe's Get() method is called, the program will throw an Exception than might be caught later.
	*@code
	*	MsiUtilities::Verify(MsiViewExecute(mViewHandle, NULL), true).Get();
	*@endcode
	*@param pErrorCode The error code to check.
	*@param mCritical [default = true] If is set to true, the program will exit if the error code is != ERROR_SUCCESS;
	*if it's false, the function will log the error and return a Maybe<void> containing the error.
	*
	*@see Maybe<void>
	*/
	static Maybe<void> Verify(unsigned int pErrorCode, bool mCritical = true);
	
	/**
	*Evaluate a <a href="https://msdn.microsoft.com/en-us/library/aa368561(v=vs.85).aspx">condition</a>.
	*
	*@param pInstallHandle The install MSIHANDLE.
	*@param pCondition A string with the condition to be evaluated.
	*@return A Maybe that might contain a bool value (the condition's result) or an error.
	*/
	static Maybe<bool> ConditionEvaluator(unsigned long pInstallHandle, const std::wstring& pCondition);

	/**
	*Reslove a <a href="https://msdn.microsoft.com/en-us/library/aa368609(v=vs.85).aspx">formatted</a> string.
	*
	*@param pInstallHandle The install MSIHANDLE
	*@param pStr The formatted string.
	*@return A Maybe that contains either the resolved string or the error describing why it failed.
	*/
	static Maybe<std::wstring> MsiUtilities::FormatString(unsigned long pInstallHandle, const std::wstring& pStr);
};