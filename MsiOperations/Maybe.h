#pragma once
#include <string>
#include <Windows.h>
#include "MsiUtilities.h"
#include <memory>

/** @file */

/**
*Enum of custom error codes.
*
*This error codes are used to return error codes where
*the Windows error codes are not sufficient.
*@see Exception
*@see Maybe
*/
enum class CustomErrorCode : unsigned int
{
	INVALID_CONVERSION, /**< The conversion failed. */
	ELEMENT_NOT_FOUND	/**< The element is not found. */
};

/**
*Wrapper over an error code and a message.
*
*The Exception class holds an error code and an error message.
*It is mainly used together with the Maybe class for error handling.
*@see CustomErrorCode
*@see Maybe
*/
class MSI_EXPORT Exception
{
public:
	/**
	*Class constructor.
	*
	*An explicit constructor used for windows error codes.
	*@param pErrorCode Windows error code.
	*@param pErrorMessage Error message.
	*@param pInnerException [optional][default = nullptr] Another exception that is kept by this object; usually a lower level exception.
	*
	*Example Usage:
	*@code
	*	Exception(ERROR_INVALID_HANDLE, L"The handle is invalid!");
	*@endcode
	*
	*@see explicit Exception(CustomErrorCode pErrorCode, std::wstring pErrorMessage = L"")
	*/
	explicit Exception(unsigned int pErrorCode, std::wstring pErrorMessage, std::unique_ptr<Exception> pInnerException = nullptr);

	/**
	*Class constructor.
	*
	*An explicit constructor used for CustomErrorCode error codes.
	*@param pErrorCode Custom error code.
	*@param pErrorMessage Error message.
	*@param pInnerException [optional][default = nullptr] Another exception that is kept by this object; usually a lower level exception.
	*
	*Example Usage:
	*@code
	*	Exception(CustomErrorCode::INVALID_CONVERSION, L"This conversion is invalid!");
	*@endcode
	*
	*@see explicit Exception(unsigned int pErrorCode, std::wstring pErrorMessage = L"")
	*/
	explicit Exception(CustomErrorCode pErrorCode, std::wstring pErrorMessage, std::unique_ptr<Exception> pInnerException = nullptr);

	/**
	*Class move constructor.
	*
	*@param pOther the object to be moved.
	*/
	Exception(Exception&& pOther);

	/**
	*Class copy constructor.
	*
	*@param pOther the object to be copy.
	*/
	Exception(const Exception& pOther);

	/**
	*Assignment move operator.
	*
	*@param pOther the object to be moved.
	*/
	Exception& operator=(Exception&& pOther);

	/**
	*Assignment move operator.
	*
	*@param pOther the object to be copy.
	*/
	Exception& operator=(const Exception& pOther);

	/**
	*Get the error code.
	*
	*@return The error code.
	*/
	unsigned int GetCode() const;

	/**
	*Get the error message.
	*
	*@return The error message.
	*/
	const std::wstring& GetMessage() const;

	/**
	*Get the inner exception.
	*
	*@return The error message.
	*/
	const Exception* const GetInnerException() const;

	/**
	*Get the error description.
	*
	*Returns a concatenated string of type "[ErrorCode]ErrorMessage".
	*@return The error description.
	*/
	std::wstring Str() const;

private:
	unsigned int mErrorCode;
	std::wstring mErrorMessage;
	std::unique_ptr<Exception> mInnerException;
};

/**
*Template class that holds either the value of type <i>T</i> or an Exception.
*
*This class is used for exception handling, so we can have a combination 
*between error codes and exceptions. <p>
*See Andrei Alexandrescu's <a href="https://github.com/ptal/expected">Expected</a> library.
*
*Example usage:
*@code
*	Maybe<int> ConvertInt(bool pError)
*	{
*		if (!pError)
*			return 42;
*		else
*			return CustomErrorCode::INVALID_CONVERSION;
*	}
*
*	Maybe<int> mbyFail = ConvertInt(false);
*@endcode
*For error checking we can use either classic error code style:
@code
*	if(mbyFail)
*	{
*		//certain no to throw, because if mbyFail would contain an error, it will not pass the condition above
*		int foo = mbyFail.Get(); 
*	}
*@endcode
*Or we can use exception handling:
*@code
*	try
*	{
*		//if mbyFail contains an error and we try to use Get(), it will throw
*		int bar = mbyFail.Get();
*	}
*	catch(Exception e)
*	{
*		//handle exception
*	}
*@endcode
*@see Maybe<void>
*@see Exception
*/
template<typename T>
class MSI_EXPORT Maybe
{
public:
	/**
	*Class constructor.
	*
	*This constructor recieves a r-value and moves it directly into the member variable.
	*@param pValue The value to save.
	*@see Maybe(const T& pValue)
	*@see Maybe(Exception pError)
	*
	*@remark T&& is a r-value, not universal reference.
	*/
	Maybe(T&& pValue) : mValue(std::move(pValue)), mError(ERROR_SUCCESS, L"") {}

	/**
	*Class constructor.
	*
	*This constructor receives a const reference and copies it into the member variable.
	*@param pValue The value to save.
	*@see Maybe(T&& pValue)
	*@see Maybe(Exception pError)
	*/
	Maybe(const T& pValue) : mValue(pValue), mError(ERROR_SUCCESS, L"") {}
	
	/**
	*Class constructor.
	*
	*This constructor receives and saves an Exception.
	*@param pError The error to save.
	*@see Maybe(const T& pValue)
	*@see Maybe(T&& pValue)
	*/
	Maybe(Exception pError) : mError(std::move(pError)) {}

	/**
	*Explicit boolean conversion operator.
	*
	*This operator is used to check if the Maybe<T> object contains an error or a value.
	*@retval true if the object contains a value
	*@retval false if the object contains an error
	*/
	explicit operator bool() { return mError.GetCode() == ERROR_SUCCESS; }

	/**
	*Getter for the internal value; Might throw.
	*
	*Returns the internal value or throws.
	*@return The internal value of type T if the object doesn't not contain an error.
	*Otherwise, if the object contains an Exception, it throws it.
	*/
	const T& Get()
	{
		if (!(*this))
			throw mError;

		return mValue;
	}

	/**
	*Getter for the internal exception.
	*
	*@return The internal exception. If no exception is set, it returns Exception(ERROR_SUCCESS).
	*/
	const Exception& GetError() const { return mError; }

private:
	T mValue;
	Exception mError;
};

/**
*Template specialization of Maybe
*
*This specialization was needed because void represents a special return case.
*
*Example Usage:
*@code
*	Maybe<void> ConvertInt(bool pError)
*	{
*		if (pError)
*			return CustomErrorCode::INVALID_CONVERSION;
*	}
*
*	//For checking the value we can use error code style:
*	if(ConvertInt(true))
*	{
*		//your code here
*	}
*
*   //or we can use exception handling:
*	try
*	{
*		ConvertInt(true).Get();
*	}
*	catch(Exception e)
*	{
*		//handle exception
*	}
*@endcode
*
*@see Maybe<T>
*@see Exception
*/
template<>
class MSI_EXPORT Maybe<void>
{
public:
	/**
	*Class constructor.
	*
	*This constructor only sets the error as ERROR_SUCCESS, since the value is void.
	*@see Maybe(const T& pValue)
	*@see Maybe(Exception pError)
	*/
	Maybe() : mError(ERROR_SUCCESS, L"") {}

	/**
	*Class constructor.
	*
	*This constructor receives and saves an Exception.
	*@param pError The error to save.
	*@see Maybe(const T& pValue)
	*@see Maybe(T&& pValue)
	*/
	Maybe(Exception pError) : mError(std::move(pError)) {}

	/**
	*Explicit boolean conversion operator.
	*
	*This operator is used to check if the Maybe<T> object contains an error or a value.
	*@retval true if the object contains a value
	*@retval false if the object contains an error
	*/
	explicit operator bool() { return mError.GetCode() == ERROR_SUCCESS; }

	/**
	*Getter for the internal value; Might throw.
	*
	*Returns the internal value or throws.
	*@return The internal value of type T if the object doesn't not contain an error.
	*Otherwise, if the object contains an Exception, it throws it.
	*/
	void Get()
	{
		if (!(*this))
			throw mError;
	}

	/**
	*Getter for the internal exception.
	*
	*@return The internal exception. If no exception is set, it returns Exception(ERROR_SUCCESS).
	*/
	const Exception& GetError() const { return mError; }

private:
	Exception mError;
};