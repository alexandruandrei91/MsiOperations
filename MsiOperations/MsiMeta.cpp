#include "MsiMeta.h"
#include <algorithm>

MsiColInfo& MsiMeta::operator[](unsigned int pColIndex)
{
	//pColIndex is 1 based
	if (pColIndex > mColsInfo.size())
	{
		MsiUtilities::CriticalError(L"Trying to access an inexistent Column Info element from Metadata!");
	}
	
	return mColsInfo[pColIndex-1];
}

const MsiColInfo& MsiMeta::operator[](unsigned int pColIndex) const
{
	//pColIndex is 1 based
	if (pColIndex > mColsInfo.size())
	{
		MsiUtilities::CriticalError(L"Trying to access an inexistent Column Info element from Metadata!");
	}

	return mColsInfo[pColIndex - 1];
}

MsiColInfo& MsiMeta::operator[](const std::wstring& pColName)
{
	auto elem = FindElem([&](const MsiColInfo& currElem){ return currElem.Name() == pColName; });
	if (!elem)
	{
		MsiUtilities::CriticalError(elem.GetError().Str());
	}

	//get the iterator from the Maybe, then dereference it
	return *(elem.Get());
}

const MsiColInfo& MsiMeta::operator[](const std::wstring& pColName) const
{
	auto elem = FindElem([&](const MsiColInfo& currElem){ return currElem.Name() == pColName; });
	if (!elem)
	{
		MsiUtilities::CriticalError(elem.GetError().Str());
	}

	//get the iterator from the Maybe, then dereference it
	return *(elem.Get());
}

Maybe<unsigned int> MsiMeta::IndexOf(const std::wstring& pColName) const
{
	auto elem = FindElem([&](const MsiColInfo& currElem){ return currElem.Name() == pColName; });
	if (!elem)
	{
		return elem.GetError();
	}

	//added 1 because the msi is 1 based
	return std::distance(std::cbegin(mColsInfo), elem.Get()) + 1;
}

std::vector<MsiColInfo> MsiMeta::GetPrimaryKeys() const
{
	std::vector<MsiColInfo> primary(mColsInfo.size());
	auto it = std::copy_if(std::begin(mColsInfo), std::end(mColsInfo), std::begin(primary),
		[](const MsiColInfo& currElem){ return currElem.IsPrimaryKey(); });
	primary.resize(std::distance(std::begin(primary), it));
	return primary;
}

std::vector<unsigned int> MsiMeta::GetPKIndices() const
{
	std::vector<unsigned int> primary;
	primary.reserve(mColsInfo.size());
	
	//find first primary key
	auto pkIt = std::find_if(std::cbegin(mColsInfo), std::cend(mColsInfo), 
		[&](const MsiColInfo& currElem){ return currElem.IsPrimaryKey(); });

	while (pkIt != std::end(mColsInfo))
	{
		//index from iterator
		primary.push_back( std::distance(std::cbegin(mColsInfo), pkIt) + 1 );
		
		//find next elements
		pkIt = std::find_if(pkIt+1, std::cend(mColsInfo),
			[&](const MsiColInfo& currElem){ return currElem.IsPrimaryKey(); });
	}

	primary.shrink_to_fit();
	return primary;
}

unsigned int MsiMeta::ColCount() const
{
	return mColsInfo.size();
}

Maybe<std::vector<MsiColInfo>::iterator> MsiMeta::FindElem(std::function<bool(const MsiColInfo&)> pLambda)
{
	auto it = std::find_if(std::begin(mColsInfo), std::end(mColsInfo), pLambda);

	if (it != std::end(mColsInfo))
		return it;
	else
		return Exception(CustomErrorCode::ELEMENT_NOT_FOUND, L"Trying to access an inexistent Column Info element from Metadata!");
}

Maybe<std::vector<MsiColInfo>::const_iterator> MsiMeta::FindElem(std::function<bool(const MsiColInfo&)> pLambda) const
{
	auto it = std::find_if(std::cbegin(mColsInfo), std::cend(mColsInfo), pLambda);

	if (it != std::end(mColsInfo))
		return it;
	else
		return Exception(CustomErrorCode::ELEMENT_NOT_FOUND, L"Trying to access an inexistent Column Info element from Metadata!");
}

