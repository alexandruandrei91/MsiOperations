#include "MsiTable.h"
#include "Maybe.h"
#include <MsiQuery.h>

MsiTable::MsiTable(const MsiDB& pDb, std::wstring pName)
	: mMsiDb(pDb), mName(pName)
{
	if (!pDb.TableExists(pName))
	{
		MsiUtilities::CriticalError(L"Table " + pName + L" doesn't exist.");
	}
	this->Execute(pDb, MsiQuery(L"SELECT * FROM " + mName));
	this->FillMeta();
	FillPrimaryKeys();
}

MsiTable::MsiTable(const MsiDB& pDb, std::wstring pName, MsiMeta pMeta)
	: mMsiDb(pDb), mName(pName)
{
	if (pDb.TableExists(pName))
	{
		MsiUtilities::CriticalError(L"Table " + pName + L" already exist.");
	}

	this->mMeta = std::move(pMeta);

	this->Execute(pDb, MsiQuery::CreateTableQuery(mName, this->mMeta));
	this->Execute(pDb, MsiQuery(L"SELECT * FROM " + mName));
}

MsiTable::MsiTable(MsiTable&& pOther)
	: MsiView(std::move(pOther)), mName(std::move(pOther.mName)), mMsiDb(pOther.mMsiDb)
{

}
MsiTable& MsiTable::operator=(MsiTable&& pOther)
{
	if (this != &pOther)
	{
		MsiView::operator=(std::move(pOther));
		mName = std::move(pOther.mName);
	}

	return *this;
}

void MsiTable::AddColumn(MsiColInfo pCol)
{
	this->Execute(mMsiDb, MsiQuery::AddColumnQuery(mName, pCol));
	this->mMeta.AddColInfo(std::move(pCol));
	this->Execute(mMsiDb, MsiQuery(L"SELECT * FROM " + mName));
}

void MsiTable::DropTable()
{
	this->Execute(mMsiDb, MsiQuery(L"DROP TABLE " + mName));
}

void MsiTable::OrderBy(std::wstring pColName)
{
	MsiQuery query(L"SELECT * FROM " + mName);
	query.OrderBy(std::move(pColName));

	this->Execute(mMsiDb, query);
}

void MsiTable::OrderBy(std::vector<std::wstring> pColNames)
{
	MsiQuery query(L"SELECT * FROM " + mName);
	query.OrderBy(std::move(pColNames));

	this->Execute(mMsiDb, query);
}

void MsiTable::FillPrimaryKeys()
{
	PMSIHANDLE recordH = 0;
	MsiUtilities::Verify(MsiDatabaseGetPrimaryKeys(mMsiDb.GetHandle(), mName.c_str(), &recordH));

	unsigned int colCount = MsiRecordGetFieldCount(recordH);
	if (colCount == -1 || colCount == 0xffffff)
	{
		MsiUtilities::CriticalError(L"The primary keys number is incorrect!");
	}

	for (unsigned int i = 1; i <= colCount; i++)
	{
		auto pkMby = GetRecordString(recordH, i);
		if (pkMby)
			mMeta[pkMby.Get()].IsPrimaryKey(true);
		else
			MsiUtilities::CriticalError(L"Could not get primary key name! " + pkMby.GetError().Str());
	}
}

const std::wstring& MsiTable::GetName() const
{
	return mName;
}