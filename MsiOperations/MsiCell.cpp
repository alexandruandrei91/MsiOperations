#include "MsiCell.h"
#include "Maybe.h"

const int MsiCell::NullInteger(0x80000000); //0x80000000 is the integer value reserved for NULL in msi; MSI_NULL_INTEGER from MsiQuery
const std::vector<char> MsiCell::NullStream(0);
const std::wstring MsiCell::NullString(L"");

MsiCell::MsiCell()
	: mType(MsiDataType::NONE)
{

}

MsiCell::MsiCell(int pIntVal)
	: mIntVal(pIntVal), mType(MsiDataType::INTEGER)
{

}

MsiCell::MsiCell(const std::vector<char>& pStreamVal)
	: mType(MsiDataType::STREAM)
{
	mStreamLen = pStreamVal.size();
	mStreamVal = new char[mStreamLen];
	std::copy(std::begin(pStreamVal), std::end(pStreamVal), mStreamVal);
}

MsiCell::MsiCell(const std::wstring& pStringVal)
	: mType(MsiDataType::STRING)
{
	mStringVal = new wchar_t[pStringVal.size() + 1];
	std::copy(std::begin(pStringVal), std::end(pStringVal), mStringVal);
	mStringVal[pStringVal.size()] = '\0';
}

MsiCell::MsiCell(const wchar_t* pStringVal)
	: MsiCell(std::wstring(pStringVal))
{

}

MsiCell::MsiCell(const MsiCell& pOther)
	: mType(pOther.mType)
{
	switch (mType)
	{
	case MsiDataType::INTEGER:
		mIntVal = pOther.mIntVal;
		break;
	case MsiDataType::STREAM:
		mStreamLen = pOther.mStreamLen;
		mStreamVal = new char[pOther.mStreamLen];
		std::copy(pOther.mStreamVal, pOther.mStreamVal + pOther.mStreamLen, mStreamVal);
		break;
	case MsiDataType::STRING:
		auto otherStrLen = std::wcslen(pOther.mStringVal) + 1; //add 1 for NULL
		mStringVal = new wchar_t[otherStrLen + 1];
		std::copy(pOther.mStringVal, pOther.mStringVal + otherStrLen, mStringVal);
		mStringVal[otherStrLen] = '\0';
		break;
	}
}

MsiCell& MsiCell::operator=(const MsiCell& pOther)
{
	//get the value from the other MsiCell. The conversions operators will not throw because, for example, 
	//if pOther.mType is INTEGER, *Maybe<int> will certainly succeed.
	
	if (this != &pOther)
	{
		switch (pOther.mType)
		{
		case MsiDataType::INTEGER:
			operator=(pOther.Int().Get());
			break;
		case MsiDataType::STREAM:
			operator=(pOther.Stream().Get());
			break;
		case MsiDataType::STRING:
			operator=(pOther.String().Get());
			break;
		case MsiDataType::NONE:
			DeleteVal();
			break;
		}

		mType = pOther.mType; //has to be after the switch because the assigments operators use mType
	}

	return *this;
}

MsiCell::MsiCell(MsiCell&& pOther)
	: mType(pOther.mType)
{
	switch (mType)
	{
	case MsiDataType::INTEGER:
		mIntVal = pOther.mIntVal;
		break;
	case MsiDataType::STREAM:
		mStreamLen = pOther.mStreamLen;
		mStreamVal = pOther.mStreamVal;
		break;
	case MsiDataType::STRING:
		mStringVal = pOther.mStringVal;
		break;
	}

	pOther.mType = MsiDataType::NONE; //set the other to NONE, so it doesn't delete the internal values moved here
}

MsiCell& MsiCell::operator=(MsiCell&& pOther)
{
	if (this != &pOther)
	{
		DeleteVal(); //clean the current object before moving into it
		mType = pOther.mType;

		switch (mType)
		{
		case MsiDataType::INTEGER:
			mIntVal = pOther.mIntVal;
			break;
		case MsiDataType::STREAM:
			mStreamLen = pOther.mStreamLen;
			mStreamVal = pOther.mStreamVal;
			break;
		case MsiDataType::STRING:
			mStringVal = pOther.mStringVal;
			break;
		}

		pOther.mType = MsiDataType::NONE; //set the other to NONE, so it doesn't delete the internal values moved here
	}

	return *this;
}

MsiCell::~MsiCell()
{
	DeleteVal();
}

MsiCell& MsiCell::operator=(int pIntVal)
{
	DeleteVal(); //clean the allocated memory before changing value

	mType = MsiDataType::INTEGER;
	mIntVal = pIntVal;
	return *this;
}

MsiCell& MsiCell::operator=(const std::vector<char>& pStreamVal)
{
	DeleteVal(); //clean the allocated memory before changing value

	mType = MsiDataType::STREAM;
	mStreamLen = pStreamVal.size();
	mStreamVal = new char[mStreamLen];
	std::copy(std::begin(pStreamVal), std::end(pStreamVal), mStreamVal);
	return *this;
}

MsiCell& MsiCell::operator=(const std::wstring& pStringVal)
{
	DeleteVal(); //clean the allocated memory before changing value

	mType = MsiDataType::STRING;
	mStringVal = new wchar_t[pStringVal.size() + 1];
	std::copy(std::begin(pStringVal), std::end(pStringVal), mStringVal);
	mStringVal[pStringVal.size()] = '\0';
	return *this;
}

Maybe<int> MsiCell::Int() const
{
	if (mType != MsiDataType::INTEGER)
		Exception(CustomErrorCode::INVALID_CONVERSION, L"The cell value requested is not an Integer!");
	
	return mIntVal;
}

Maybe<std::vector<char>> MsiCell::Stream() const
{
	if (mType != MsiDataType::STREAM)
		Exception(CustomErrorCode::INVALID_CONVERSION, L"The cell value requested is not a Stream!");
	return std::vector<char>(mStreamVal, mStreamVal + mStreamLen);
}

Maybe<std::wstring> MsiCell::String() const
{
	if (mType != MsiDataType::STRING)
		Exception(CustomErrorCode::INVALID_CONVERSION, L"The cell value requested is not a String!");
	return std::wstring(mStringVal);
}

MsiDataType MsiCell::Type() const
{
	return mType;
}


bool MsiCell::operator==(const MsiCell& pOther)
{
	if (mType != pOther.mType)
		return false;

	switch (mType)
	{
	case MsiDataType::INTEGER:
		return mIntVal == pOther.mIntVal;
	case MsiDataType::STREAM:
		if (mStreamLen != pOther.mStreamLen)
			return false;
		return !std::memcmp(mStreamVal, pOther.mStreamVal, mStreamLen * sizeof(wchar_t));
	case MsiDataType::STRING:
		return !std::wcscmp(mStringVal, pOther.mStringVal);
	}

	//both are none
	return true;
}

bool MsiCell::operator!=(const MsiCell& pOther)
{
	return !operator==(pOther);
}

void MsiCell::DeleteVal()
{
	if (mType == MsiDataType::STREAM)
		delete[] mStreamVal;
	else if (mType == MsiDataType::STRING)
		delete[] mStringVal;
}
