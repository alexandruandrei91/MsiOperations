#pragma once
#include "MsiFetchFilters.h"
#include <unordered_map>
class MsiCells;

/**@file*/

/**
*Class containing only static methods used to construct common MsiFetchFilters::CallableFilter objects.
*
*This class contains helper functions used to construct callable objects that will be added to a MsiFetchFilters
*collection and further sent to MsiView::Fetch() to filter the fetch results.
*@see MsiFetchFilters
*@see MsiView
*/
class MSI_EXPORT MsiCommonFilters
{
public:
	/**
	*Deleted default constructor.
	*
	*Since this class only contains static helper functions, we don't need to construct it.
	*/
	MsiCommonFilters() = delete; 

	/**
	*Get a filter that will match the specified primary key.
	*
	*@param pPk Primary Key to match.
	*/
	static MsiFetchFilters::CallableFilter MatchPrimaryKey(const MsiCells& pPk);

	/**
	*Get a filter that will match the specified columns.
	*
	*For example, if we want a filter to match where the column 3 is "abc" and the column 5 is 123, we
	*can call this function like this:
	*@code
	*	MatchCells({ {3, L"abc"}, {5, 123} });
	*@endcode
	*@param pCells A map of MsiCells where the key is the cell's index.
	*/
	static MsiFetchFilters::CallableFilter MatchCells(const std::unordered_map<unsigned int, MsiCell>& pCells);

	/**
	*Get a filter that will match the specified columns.
	*
	*For example, if we want a filter to match where the column "Name" is "abc" and the column "Id" is 123, we
	*can call this function like this:
	*@code
	*	MatchCells({ {L"Name", L"abc"}, {L"Id", 123} });
	*@endcode
	*@param pCells A map of MsiCells where the key is the cell's name.
	*/
	static MsiFetchFilters::CallableFilter MatchCells(const std::unordered_map<std::wstring, MsiCell>& pCells);
};

