#pragma once

#include "MsiUtilities.h"
#include <Windows.h>
#include <Msi.h>
#include <string>
#include <vector>

/** @file */

//forward declared to avoid circular inclusion
class MsiTable;
class MsiMeta;

/**
*Represents a Msi Database.
*
*This class is a wrapper over a Msi Database handle and it offers the possibility
*to open a Msi from disc or get a Msi Handle from a running instance (as a custom action).
*When this object is destroyed, it saves the modifications in the Msi Database and closes it.
*Also, it offers the possibility to get a list with the existing tables and to check if a
*specific table exists.
*/
class MSI_EXPORT MsiDB
{
private:
	struct MsiSummaryInfo
	{
		unsigned int DataType;
		int IntVal;
		FILETIME FileVal;
		std::wstring StrVal;
	};

public:

	/**
	*Class constructor.
	*
	*This constructor is used to open a database from disc.	
	*@code
	*	MsiDb(L"C:\\path\\to\\the\\msi\\file.msi");
	*@endcode
	*@param pMsiPath The msi path (windows format).
	*@see MsiDB(MSIHANDLE pInstallHandle);
	*/
	MsiDB(std::wstring pMsiPath);

	/**
	*Class constructor.
	*
	*This constructor is used to open a database from an install handle (custom actions).
	*@param pInstallHandle Install handle received in the custom action.
	*@see MsiDB(std::wstring pMsiPath);
	*/
	MsiDB(MSIHANDLE pInstallHandle);

	/**Deleted copy constructor.*/
	MsiDB(const MsiDB&) = delete;
	/**Deleted move constructor.*/
	MsiDB(MsiDB&&) = delete;
	/**Deleted copy assignment operator.*/
	MsiDB& operator=(const MsiDB&) = delete;
	/**Deleted move assignment operator.*/
	MsiDB& operator=(MsiDB&&) = delete;

	/**
	*Class destructor.
	* 
	*It saves the Msi modifications and closes the database.
	*/
	~MsiDB();

	/**
	*Getter for the Msi Database handle.
	*
	*@return the Msi Database handle.
	*/
	MSIHANDLE GetHandle() const;

	/**
	*Check if the table exists in the database.
	*
	*@param pTableName Name of the searched table.
	*@retval true The table exists.
	*@retval false The table does not exists.
	*@remark The function will return false for the system table "_Tables" although this table exists.
	*/
	bool TableExists(const std::wstring& pTableName) const;

	/**
	*Get a vector containing the names of all tables from the database.
	*
	*@return A  a vector containing the names of all tables.
	*@remark The vector will not contain the system table "_Tables" although this table exists.
	*/
	std::vector<std::wstring> GetAllTablesNames() const;

	/**
	*Get the summary information for the package.
	*/
	std::vector<MsiSummaryInfo> GetSummaryInfo();

	/**
	*Set the summary information for the package.
	*/
	void SetSummaryInfo(std::vector<MsiSummaryInfo> pSummaryInfo);

private:
	std::wstring mMsiPath;
	MSIHANDLE mDBHandle;
};

