#pragma once
#include "MsiColInfo.h"
#include <vector>
#include "Maybe.h"
#include <functional>

/** @file */

/**
*Represents the schema for a MsiTable or MsiView.
*
*It's a collection of MsiColInfo elements that represents columns
*informations for a table or a view.
*/
class MSI_EXPORT MsiMeta
{
public:
	/**
	*Class constructor.
	*
	*Default constructor used in declarations without initialization.
	*
	*@see MsiMeta(std::vector<MsiColInfo> pColsInfo)
	*/
	MsiMeta(){};

	/**
	*Class constructor.
	*
	*It receives a variable number of MsiColInfo elements that represents a view's schema.
	*
	*@code
	*	MsiMeta meta(	{ L"Id", MsiDataType::INTEGER, 0, true, false, false },
	*					{ L"FileName", MsiDataType::STRING, 255, false, false, true },
	*					{ L"FileContent", MsiDataType::STREAM, 0, false, true, false }
	*				);
	*@endcode
	*
	*@see MsiColInfo
	*/
	template<typename... Args>
	MsiMeta(MsiColInfo pColInfo, Args&&... pColInfos);
	
	/**
	*Adds column descriptions to the metadata.
	*
	*It receives a variable number of MsiColInfo elements that represents a view's schema.
	*
	*@see MsiMeta(MsiColInfo pColInfo, Args&&... pColInfos)
	*/
	template<typename T, typename... Args>
	void AddColInfo(T&& pColInfo, Args&&... pColInfos);

	/**
	*Operator overloading to easily access or modify each MsiColInfo in the metadata by index.
	*
	*@param pColIndex The column index. Starts from 1.
	*@return It returns a MsiColInfo by reference, so it can be modified.
	*
	*@remark The index is one based!
	*@remark If the index is invalid, the program will exit.
	*/
	MsiColInfo& operator[](unsigned int pColIndex);

	/**
	*Operator overloading for easier access to each MsiColInfo in the metadata by index.
	*
	*@param pColIndex The column index. Starts from 1.
	*@return It returns a MsiColInfo by value.
	*
	*@remark The index is one based!
	*@remark If the index is invalid, the program will exit.
	*/
	const MsiColInfo& operator[](unsigned int pColIndex) const;

	/**
	*Operator overloading to easily access or modify each MsiColInfo in the metadata by name.
	*
	*@param pColName The column name.
	*@remark If the name is invalid, the program will exit.
	*/
	MsiColInfo& operator[](const std::wstring& pColName);

	/**
	*Operator overloading for easier access to each MsiColInfo in the metadata by name.
	*
	*@param pColName The column name.
	*@return It returns a MsiColInfo by value.
	*
	*@remark If the name is invalid, the program will exit.
	*/
	const MsiColInfo& operator[](const std::wstring& pColName) const;

	/**
	*Get the index of a column.
	*
	*This function is used to retrieve the 1 based index from the metadata, by specifying the column's name.
	*@param pColName The column name.
	*@return A Maybe that contains either the index of the column or an error.
	*@remark The index starts from 1.
	*/
	Maybe<unsigned int> IndexOf(const std::wstring& pColName) const;

	/**
	*Get a vector containing the primary keys column infos.
	*
	*@return A vector containing all the MsiColInfo that are primary keys.
	*/
	std::vector<MsiColInfo> GetPrimaryKeys() const;

	/**
	*Get a vector containing the primary keys indices.
	*
	*@return A vector containing all primary keys indices.
	*/
	std::vector<unsigned int> GetPKIndices() const;

	/**
	*Get the number of columns in the metadata.
	*
	*@return The number of columns in the metadata.
	*/
	unsigned int ColCount() const;

private:
	void AddColInfo() {}

	Maybe<std::vector<MsiColInfo>::iterator> FindElem(std::function<bool(const MsiColInfo&)> pLambda);
	Maybe<std::vector<MsiColInfo>::const_iterator> FindElem(std::function<bool(const MsiColInfo&)> pLambda) const;

	std::vector<MsiColInfo> mColsInfo;
};

#include "MsiMeta.hpp"