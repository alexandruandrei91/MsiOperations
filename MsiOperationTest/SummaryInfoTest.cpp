#include "MsiDB.h"
#include <string>

bool SummaryInfoTest(const std::wstring& pMsiPath)
{
	MsiDB msiDb(pMsiPath);

	auto summary = msiDb.GetSummaryInfo();
	msiDb.SetSummaryInfo(summary);

	return true;
}