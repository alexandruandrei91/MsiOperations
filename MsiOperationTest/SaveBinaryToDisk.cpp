#include "Maybe.h"
#include "MsiDB.h"
#include "MsiTable.h"
#include "MsiCommonFilters.h"
#include <string>
#include <fstream>

bool SaveBinaryToDisk(const std::wstring& pMsiPath, const std::wstring& pBinPK)
{
	
	MsiDB msiDb(pMsiPath);

	MsiTable msiTable(msiDb, L"Binary");

	MsiFetchFilters filters;
	filters.AddFilters( MsiCommonFilters::MatchPrimaryKey({ pBinPK }) );

	if (msiTable.Fetch(filters))
	{
		std::ofstream f(pBinPK);

		auto index = msiTable.GetMeta().IndexOf(L"Data");
		if (index)
		{
			Maybe<std::vector<char>> stream = msiTable.GetCurrentRow()[index.Get()].Stream();
			if (stream)
			{
				f.write(stream.Get().data(), stream.Get().size());
				f.close();
			}
			else
			{
				MsiUtilities::LogInfo(L"Could not retrieve the stream from Data cell.");
				f.close();
				return false;
			}
		}
		else
		{
			MsiUtilities::LogInfo(L"Could not find the column 'Data': " + index.GetError().GetMessage());
			return false;			
		}
	}
	else
	{
		MsiUtilities::LogInfo(L"The primary key " + pBinPK + L" was not found!");
		return false;
	}
	
	return true;
}