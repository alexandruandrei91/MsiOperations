#include <string>

extern bool SaveBinaryToDisk(const std::wstring& pMsiPath, const std::wstring& pBinPK);

extern bool ComponentSelector(const std::wstring& pMsiPath, const std::wstring& pComponent);

extern bool OrderByTest(const std::wstring& pMsiPath);

extern bool SelectTest(const std::wstring& pMsiPath);

extern bool AddComboBox(const std::wstring& pMsiPath);

extern bool AddListBox(const std::wstring& pMsiPath);

extern bool SummaryInfoTest(const std::wstring& pMsiPath);