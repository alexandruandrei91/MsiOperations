#include "MsiDB.h"
#include "MsiTable.h"
#include "MsiColOperations.h"
#include <string>

bool AddComboBox(const std::wstring& pMsiPath)
{
	MsiDB msiDb(pMsiPath);

	const std::wstring dialog = L"WelcomeDlg";
	const std::wstring comboName = L"MyCombo";
	
	//add combo to dialog
	MsiTable controlT(msiDb, L"Control");
	controlT.UpdateCurrRow({ dialog, comboName, L"ComboBox", 125, 125, 100, 100, 3, L"COMBO", MsiCell::NullString, MsiCell::NullString, MsiCell::NullString });
	
	//create querry to get all the id's of the controlls in the dialog
	MsiColOp dlg_c(controlT, L"Dialog_");
	MsiColOp ctrl_c(controlT, L"Control");

	MsiQuery query = MsiQuery::SelectQuery(
	{ controlT },
	dlg_c == dialog && ctrl_c != comboName,
	{ ctrl_c }
	);
	query.OrderBy(L"Control");

	MsiView view(msiDb, query);

	MsiTable coboT(msiDb, L"ComboBox");

	auto ctrlIndex = view.GetMeta().IndexOf(L"Control");
	if (ctrlIndex)
	{
		int index = 1;
		while (view.Fetch())
		{
			const auto& currentRow = view.GetCurrentRow();
			coboT.UpdateCurrRow({ L"COMBO", index++, currentRow[ctrlIndex.Get()], currentRow[ctrlIndex.Get()] });
		}
	}
	else
	{
		MsiUtilities::LogInfo(L"Could not find column Control");
		return false;
	}

	return true;
}