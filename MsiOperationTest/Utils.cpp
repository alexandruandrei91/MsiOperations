#include <ostream>
#include <iostream>
#include <iomanip>
#include "Utils.h"

void PrintRow(const MsiRow& row, const MsiMeta& meta)
{
	for (unsigned int i = 1; i <= meta.ColCount(); i++)
	{
		if (meta[i].Type() == MsiDataType::INTEGER)
		{
			std::wcout << std::setw(20) << row[i].Int().Get() << "           ";
		}
		else if (meta[i].Type() == MsiDataType::STRING)
		{
			std::wcout << std::setw(20) << row[i].String().Get() << "           ";
		}
		else if (meta[i].Type() == MsiDataType::STREAM)
		{
			//std::wcout << std::setw(20) << " ";
			for (char elem : static_cast<std::vector<char>>(row[i].Stream().Get()))
				std::cout << elem;
		}
	}
	std::cout << std::endl;
}

void PrintView(MsiView& msiView, int max)
{
	auto meta = msiView.GetMeta();
	for (unsigned int i = 1; i <= meta.ColCount(); i++)
	{
		std::wcout << std::setw(20) << meta[i].Name() << "           ";
	}
	
	std::cout << std::endl;
	
	for (const auto& row : msiView.GetAllRows())
	{
		PrintRow(row, meta);
		if (--max == 0)
			break;
	}
}